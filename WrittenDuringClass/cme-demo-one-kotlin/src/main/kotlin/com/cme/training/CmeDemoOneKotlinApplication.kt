package com.cme.training

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.*

class Person(var name: String, var age: Int) {
	constructor(): this("", 0)
}

@Configuration
class MyConfig {
	@Bean
	fun f1() = mutableListOf(
				Person("Fred", 30),
				Person("Jane", 31),
				Person("Pete", 32),
				Person("Mary", 33),
				Person("Dave", 34))
}

@SpringBootApplication
class CmeDemoOneKotlinApplication

fun main(args: Array<String>) {
	runApplication<CmeDemoOneKotlinApplication>(*args)
}

@RestController
@RequestMapping("/person")
class MyService(val people: MutableList<Person>) {

	@GetMapping(produces = ["application/json"])
	fun f1() = people

	@PostMapping(consumes = ["application/json"],
				 produces = ["text/plain"])
	fun f3(@RequestBody input: Person): String {
		people.add(input)
		return "OK"
	}

	@GetMapping(value = ["/{name}"], produces = ["application/json"])
	fun f2(@PathVariable name: String) = people.find { it.name == name }
}

