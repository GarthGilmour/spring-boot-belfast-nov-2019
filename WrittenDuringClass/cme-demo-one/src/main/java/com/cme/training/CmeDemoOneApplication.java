package com.cme.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmeDemoOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmeDemoOneApplication.class, args);
	}

}
