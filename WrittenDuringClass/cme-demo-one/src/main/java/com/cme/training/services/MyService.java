package com.cme.training.services;

import com.cme.training.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/person")
public class MyService {
    @Autowired
    private List<Person> people;

    @GetMapping(produces = "application/json")
    public List<Person> f1() {
        return people;
    }
    @PostMapping(consumes = "application/json", produces="text/plain")
    public String f3(@RequestBody Person input) {
        people.add(input);
        return "OK";
    }
    @GetMapping(value="/{name}",produces = "application/json")
    public Person f2(@PathVariable String name) {
        return people.stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElse(new Person("unknown", 0));
    }
}
