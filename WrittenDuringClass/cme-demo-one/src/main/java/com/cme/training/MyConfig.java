package com.cme.training;

import com.cme.training.model.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class MyConfig {
    @Bean
    public List<Person> f1() {
        return new ArrayList<>(Arrays.asList(
                new Person("Fred",30),
                new Person("Jane",31),
                new Person("Pete",32),
                new Person("Mary",33),
                new Person("Dave",34)
        ));
    }
}
