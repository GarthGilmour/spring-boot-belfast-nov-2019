package com.instil.controllers;

import com.instil.model.Course;
import com.instil.model.CourseServiceKeyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("courses-kv")
public class CourseControllerKeyValue {
    private CourseServiceKeyValue courseService;

    @Autowired
    public CourseControllerKeyValue(CourseServiceKeyValue courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public Flux<Course> all() {
        return courseService.get();
    }

    @DeleteMapping("/{id}")
    public Mono<Long> delete(@PathVariable("id") String id) {
        return courseService.delete(id);
    }


    @GetMapping("/{id}")
    public Mono<Course> getById(@PathVariable("id") String id) {
        return courseService.get(id);
    }

    @PostMapping("/reset")
    public Flux<Boolean> reset() {
        return courseService.populate();
    }
}
