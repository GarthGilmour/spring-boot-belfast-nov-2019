package com.instil.controllers;

import com.instil.model.Course;
import com.instil.model.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//This controller will be synchronous - not reactive
@RestController
@RequestMapping("/courses-jpa")
public class CourseControllerJPA {
    private CourseRepository courseService;
    private List<Course> portfolio;

    public CourseControllerJPA(CourseRepository courseService,
                               List<Course> portfolio) {
        this.courseService = courseService;
        this.portfolio = portfolio;
    }

    @GetMapping
    public Iterable<Course> all() {
        List<Course> findAll = new ArrayList<>();
        return courseService.findAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        courseService.deleteById(id);
    }

    @GetMapping("/{id}")
    public Optional<Course> getById(@PathVariable("id") String id) {
        return courseService.findById(id);
    }

    @GetMapping("/byKeyword/{search}")
    public Iterable<Course> getByKeyword(@PathVariable("search") String search) {
        return courseService.findByTitleIgnoreCase(search);
    }

    @PostMapping("/reset")
    public void reset() {
        courseService.deleteAll();
        courseService.saveAll(portfolio);
    }
}
