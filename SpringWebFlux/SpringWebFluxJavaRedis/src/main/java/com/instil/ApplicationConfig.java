package com.instil;

import com.instil.model.Course;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.instil.model.CourseDifficulty.*;

@Configuration
@EnableRedisRepositories
public class ApplicationConfig {

    @Bean
    public ReactiveRedisOperations<String, Course> redisOperations(ReactiveRedisConnectionFactory factory) {
        Jackson2JsonRedisSerializer<Course> serializer = new Jackson2JsonRedisSerializer<>(Course.class);

        RedisSerializationContext.RedisSerializationContextBuilder<String, Course> builder = RedisSerializationContext.newSerializationContext(new StringRedisSerializer());

        RedisSerializationContext<String,Course> context = builder.value(serializer).build();

        return new ReactiveRedisTemplate<>(factory, context);
    }

    @Bean
    public ReactiveRedisOperations<String, String> stringOperations(ReactiveRedisConnectionFactory factory) {
        return new ReactiveRedisTemplate<>(factory, RedisSerializationContext.string());
    }

    @Bean(name = "portfolio")
    @Scope("prototype")
    public List<Course> buildPortfolio() {
        List<Course> portfolio = new ArrayList<>();
        portfolio.add(new Course("AB12", "Programming in Scala", BEGINNER, 4));
        portfolio.add(new Course("CD34", "Machine Learning in Python", INTERMEDIATE, 3));
        portfolio.add(new Course("EF56", "Advanced Kotlin Coding", ADVANCED, 2));
        portfolio.add(new Course("GH78", "Intro to Domain Driven Design", BEGINNER, 3));
        portfolio.add(new Course("IJ90", "Database Access with JPA", INTERMEDIATE, 3));
        portfolio.add(new Course("KL12", "Functional Design Patterns in F#", ADVANCED, 2));
        portfolio.add(new Course("MN34", "Building Web UIs with Angular", BEGINNER, 4));
        portfolio.add(new Course("OP56", "Version Control with Git", INTERMEDIATE, 1));
        portfolio.add(new Course("QR78", "SQL Server Masterclass", ADVANCED, 2));
        portfolio.add(new Course("ST90", "Go Programming for Beginners", BEGINNER, 5));
        portfolio.add(new Course("UV12", "Coding with Lock Free Algorithms", INTERMEDIATE, 2));
        portfolio.add(new Course("WX34", "Coaching Skills for SCRUM Masters", ADVANCED, 3));
        return portfolio;
    }
}
