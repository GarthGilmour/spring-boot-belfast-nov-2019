package com.instil.controllers

import com.instil.model.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("courses-kv")
class CourseControllerKeyValue(@Autowired val courseService: CourseServiceKeyValue) {
    @GetMapping
    fun all(): Flux<Course> {
        return courseService.get()
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: String): Mono<Long> {
        return courseService.delete(id)
    }


    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: String): Mono<Course> {
        return courseService.get(id)
    }

    @PostMapping("/reset")
    fun reset(): Flux<Boolean> {
        return courseService.populate()
    }
}